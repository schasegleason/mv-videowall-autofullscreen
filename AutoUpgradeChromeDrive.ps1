## Check and Install Chrome and Selenium Chrome Driver Updates 
## The install file for the ChromeDrive should be listed under "CHROMEDRIVERPATH" in the .cfg file. 
## Default Path CHROMEDRIVERPATH:C:\ProgramData\chocolatey\lib\chromedriver\tools\
choco upgrade chromedriver -y 



## For MACs use
## Default path /usr/local/bin/chromedriver/
## Cant open ChromeDriver checkout - https://support.apple.com/en-us/HT202491
brew upgrade chromedriver 


## Linux
apt upgrade chromedriver -y
yum upgrade chromedriver -y
