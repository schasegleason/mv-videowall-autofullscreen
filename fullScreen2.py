from os import system
import random
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from time import sleep
import subprocess
import re


#RegEx to Search Config File
url = re.compile('(URL:)(.+.meraki.com.+)')
uname = re.compile('(USERNAME:)(.+)')
paswd = re.compile('(PASSWORD:)(.+)')
refresh = re.compile('(REFRESH:)(.+)')
org = re.compile('(ORG:)(.+)')
crmdrv = re.compile('(CHROMEDRIVERPATH:)(.+)')




def openFullScreen():
	videoWallURL = ""
	userName = ""
	passWord = ""
	oRg = ""
	reFresh = 2700 #Default to 45 min refresh rates
	crmdrvPath = ""

	#Removes the Automated Software InfoBar, and Disables the Developer Extentsion warning 
	chrome_options = webdriver.ChromeOptions(); 
	chrome_options.add_experimental_option("excludeSwitches", ['enable-automation', 'load-extension']);

	

	#Open Config File and assign vars 
	with open(('fullScreen.cfg'), 'r') as inputFile:
		for line in inputFile:
			print(line)
			if url.match(line):
				print("MATCH")
				videoWallURL = url.match(line).group(2)
			if uname.match(line):
				userName = uname.match(line).group(2)
			if paswd.match(line):
				passWord = paswd.match(line).group(2)
			if refresh.match(line):
				reFresh = refresh.match(line).group(2)
			if org.match(line):
				oRg = org.match(line).group(2)
			if crmdrv.match(line):
				crmdrvPath = crmdrv.match(line).group(2)

	print (videoWallURL)
	print (userName)
	#print (passWord)
	print (reFresh)
	print (oRg)


	if crmdrvPath != "":
		#Assume if not chromedriver path given it is in run dir
		driver = webdriver.Chrome('chromedriver.exe', options=chrome_options)
	else:
		driver = webdriver.Chrome(crmdrvPath+'chromedriver.exe', options=chrome_options)

	driver.get(videoWallURL)
	sleep(1)
	driver.find_element_by_xpath('//*[@id="email"]').send_keys(userName)
	sleep(1) 
	nextbtn = driver.find_element_by_id("next-btn").click()
	sleep(10)

	#driver.find_element_by_xpath('//*[@id="email"]').send_keys(userName)
	driver.find_element_by_xpath('//*[@id="password"]').send_keys(passWord) 

	driver.find_element_by_xpath('//*[@id="login-btn"]').click()
#//*[@id="video_wall_wrapper"]/div/div/div[2]/div[2]/div[1]/div/div[1]/div[2]/button
# //*[@id="video_wall_wrapper"]/div/div/div[2]/div[2]/div[1]/div/div[1]/div[2]/button
#video_wall_wrapper > div > div > div.VideoWallView > div.VideoWallLayout.horizontalFullBleed > div:nth-child(1) > div > div.VideoPlayerTopControls > div.right > button
#//*[@id="video_wall_wrapper"]/div/div/div[2]/div[2]/div[1]/div/div[1]/div[2]/button/text()
	#If not Org Set do not try to click it
	if oRg != "":
		xpath = "//*[contains(text(),\""+oRg+"\")]"
		#print(xpath)
		driver.find_element_by_xpath(xpath).click()
		sleep(5)

	#driver.find_element_by_xpath('//*[@id="video_wall_wrapper"]/div/div/div[2]/div[1]/div/div[2]/button').click()
	#driver.find_element(By.XPATH, '//button[text()="Some text"]')
	#driver.find_element(By.XPATH, '//*button[text()="Fullscreen"]').click()
	#driver.find_element_by_xpath('//*[@id="video_wall_wrapper"]/div/div/div[2]/div[2]/div[1]/div/div[1]/div[2]/button').click()
	driver.find_element_by_xpath("//*[text()='Fullscreen']").click();
     
	sleep(reFresh) #Sleep the Refresh time the first lunch 

	while True:
	  driver.refresh()
	  sleep(5) #Wait for Page to Load
	  #driver.find_element_by_xpath('//*[@id="video_wall_wrapper"]/div/div/div[2]/div[1]/div/div[2]/button').click() 
	  driver.find_element_by_xpath("//*[text()='Fullscreen']").click();

	  sleep(reFresh)
	 
while True:

	try:
		openFullScreen()
	except Exception as e:
		openFullScreen()
# ToDo: Close out of the window and print error log

##Auto-py-to-exe