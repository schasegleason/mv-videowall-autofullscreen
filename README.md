# MV-VideoWall-AutoFullScreen

Auto FullScreens Meraki Video Walls 

Web Browsers do not allow you to programmatically call the FullScreen function. 

Selenium WebDrive (typically used for QA) allows you interactive with a browser programmatically.

Using Selenium we are able to script the process of Logging into the Meraki Dashboard, and Selecting the Full Screen Button of a Video Wall enabling a KISOK / Touch free way to view video walls. 


There are several compoents needed to accomplish this task
- EXE is just a precompiled version of the Python Script
- Python script can be run manually but will need Python3 and all need modules installed


Check out the Config file this is where all the varibles of the script are defined. 
URL, Username, Password, ORG, and Location of the WebDriver. 

Also note as Chrome updates the Selenium drive will need to be updated as well, it is recommend to check for updates of the Webdrive during each reboot. 
